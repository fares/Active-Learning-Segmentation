from strategies import *
import tqdm
import pytorch_lightning as pl
import segmentation_models_pytorch as smp
import os
import torch
import torch.nn as nn
from PIL import Image
import torchvision.transforms as T
import torchvision.transforms.functional as TF
from torch.utils.data import DataLoader, Dataset
import pandas as pd
import time
import json
import wandb
from unet_model import *
import pickle
import matplotlib.pyplot as plt
import cv2
import supervision as sv
from scipy.spatial.distance import cdist
import torch.optim as optim
from dbscan import Similarities
import numpy as np


class CustomDataset(Dataset):
    # def __init__(self, df, with_logits, normalization=False, denormalization=False):
    def __init__(self, X, Y, logits, image_dim, with_logits, normalization=False, denormalization=False):    
        # self.df = df
        self.X = X
        self.Y = Y
        self.LOGITS = logits
        self.with_logits = with_logits
        self.image_dim=image_dim
        self.normalization=normalization
        self.denormalization=denormalization

    def __len__(self):
        # return len(self.df)
        return len(self.X)

    def __resize__(self, x):
        return cv2.resize(x, (self.image_dim,self.image_dim), interpolation=cv2.INTER_CUBIC)

    def __transform__(self, image, gt_mask, logits):
        image = torch.Tensor(image).view(3,self.image_dim,self.image_dim)
        gt_mask = torch.Tensor(gt_mask).view(1,self.image_dim,self.image_dim)
        
        if self.with_logits:
            logits = torch.Tensor(logits).view(1,self.image_dim,self.image_dim)

        if self.normalization:
            image/=255        

        if self.denormalization:
            image*=255
        return image, gt_mask, logits

    def __getitem__(self, index):                     
        # image = np.load(self.df["images"][index], allow_pickle=True)
        # gt_mask = np.load(self.df["masks"][index], allow_pickle=True)
        image = np.load(self.X[index], allow_pickle=True)
        gt_mask = np.load(self.Y[index], allow_pickle=True)
        if image.shape[1] != self.image_dim:
            image = self.__resize__(image)
        if gt_mask.shape[1] != self.image_dim:
            gt_mask = self.__resize__(gt_mask)

        if self.with_logits:
            logits = np.load(self.LOGITS[index], allow_pickle=True)
            if logits.shape[1] != self.image_dim:
                logits = self.__resize__(logits)
        else:
            logits = 0
            
        image, gt_mask, logits = self.__transform__(image, gt_mask, logits)
        gt_mask = gt_mask.clamp(max=1.0)
        return image, gt_mask, logits, index


class Data:
    def __init__(self, img_train, label_train, logits_train, img_test, label_test, handler, image_dim=128, with_logits=True):
        """
        Data management class for handling seen and unseen data.

        Args:
            img_train (list): List of training image file paths.
            label_train (list): List of training mask file paths.
            logits_train (list): List of training logits file paths.
            img_test (list): List of test image file paths.
            label_test (list): List of test mask file paths.
            handler (Handler): Instance of the Handler class for data transformation.
        """
        self.img_train = img_train
        self.label_train = label_train
        self.logits_train = logits_train
        self.img_test = img_test
        self.label_test = label_test
        self.handler = handler
        self.image_dim = image_dim
        self.with_logits = with_logits
        self.n_pool = len(img_train)
        self.n_test = len(img_test)

        self.seen_idxs = np.zeros(self.n_pool, dtype=bool)
        self.similarity = Similarities()

    def initialize_labels(self, num):
        """
        Initialize the seen pool by randomly selecting a given number of samples.

        Args:
            num (int): Number of samples to initialize as seen.

        Returns:
            None
        """
        # Generate initial seen pool
        # tmp_idxs = np.arange(self.n_pool)
        # np.random.shuffle(tmp_idxs)
        # self.seen_idxs[tmp_idxs[:num]] = True
        self.seen_idxs[:num] = True

    def get_seen_data(self):
        """
        Get the seen data and their corresponding indices.

        Returns:
            tuple: seen indices and transformed seen data.
        """
        seen_idxs = np.arange(self.n_pool)[self.seen_idxs]
        seen_X = [self.img_train[idx] for idx in seen_idxs]
        seen_Y = [self.label_train[idx] for idx in seen_idxs]
        seen_logits = [self.logits_train[idx] for idx in seen_idxs]
        
        return seen_idxs, self.handler(seen_X, seen_Y, seen_logits, image_dim=self.image_dim ,with_logits=self.with_logits)

    def get_unseen_data(self):
        """
        Get the unseen data and their corresponding indices.

        Returns:
            tuple: unseen indices and transformed unseen data.
        """
        unseen_idxs = np.arange(self.n_pool)[~self.seen_idxs]
        unseen_X = [self.img_train[idx] for idx in unseen_idxs]
        unseen_Y = [self.label_train[idx] for idx in unseen_idxs]
        unseen_logits = [self.logits_train[idx] for idx in unseen_idxs]
        return unseen_idxs, self.handler(unseen_X, unseen_Y, unseen_logits, image_dim=self.image_dim ,with_logits=self.with_logits)

    def get_train_data(self):
        """
        Get the training data (seen + unseen) and their corresponding indices.

        Returns:
            tuple: seen indices and transformed training data.
        """
        return self.seen_idxs.copy(), self.handler(self.img_train, self.label_train, self.logits_train, image_dim=self.image_dim ,with_logits=self.with_logits)

    def get_test_data(self):
        """
        Get the test data and their corresponding indices.

        Returns:
            tuple: Transformed test data.
        """
        return self.handler(self.img_test, self.label_test, [], image_dim=self.image_dim, with_logits=False)

    def cal_test_metrics(self, logits, mask):
        """
        Calculate evaluation metrics for the test data.

        Args:
            logits (torch.Tensor): Logits output of the model.
            mask (torch.Tensor): Ground truth mask.

        Returns:
            tuple: Intersection over Union (IoU), DiceLoss, cosine_similarity, eculidian_distance and F1-score.
        """
        prob_mask = logits.sigmoid()
        pred_mask = (prob_mask > 0.5).float()

        # Compute true positive, false positive, false negative, and true negative 'pixels' for each class
        tp, fp, fn, tn = smp.metrics.get_stats(pred_mask.long(), mask.long(), mode="binary")
        # Calculate IoU and F1-score
        iou = smp.metrics.iou_score(tp, fp, fn, tn, reduction="micro")
        # accuracy = smp.metrics.accuracy(tp, fp, fn, tn, reduction="micro")
        f1 = smp.metrics.f1_score(tp, fp, fn, tn, reduction="micro")
        # recall = smp.metrics.recall(tp, fp, fn, tn, reduction="micro")
        # precision = smp.metrics.precision(tp, fp, fn, tn, reduction="micro")
        loss = smp.losses.DiceLoss(smp.losses.BINARY_MODE, from_logits=True)
        dice_loss = loss(logits, mask)
        cosine_similarity = self.similarity.cosine_similarity(pred_mask.long(), mask.long())
        eculidian_distance = self.similarity.eculidian_distance(pred_mask.long(), mask.long())

        return iou, dice_loss, cosine_similarity, eculidian_distance, f1
        

class KnowledgeDistillation():
    def __init__(self, model, params):
        self.model = model
        self.device = torch.device("cuda")
        self.image_dim = params["img_size"][0]
        self.params=params
        
    def get_similarity(self, mask, gt_mask):
        if not torch.is_tensor(mask):
            mask = torch.tensor(mask)
        if not torch.is_tensor(gt_mask):
            gt_mask = torch.tensor(gt_mask)
            
        tp, fp, fn, tn = smp.metrics.get_stats(mask.long(), gt_mask.long(), mode="binary", threshold=0.5)
        iou = smp.metrics.iou_score(tp, fp, fn, tn, reduction="micro")
        return iou 

    def test(self, test_loader):
        self.clf.eval()
    
        scores=[]
        with torch.no_grad():
            for inputs, labels, _, _ in test_loader:
                inputs, labels = inputs.to(self.device), labels.to(self.device)
    
                outputs = self.clf(inputs)
                prob_mask = outputs.sigmoid()
                pred_mask = (prob_mask > 0.5).float()
                iou = self.get_similarity(pred_mask, labels)
                scores.append(iou)
    
        iou_score = sum(scores)/len(scores)
        # print(f"Test IoU_Score: {iou_score:.2f}")
        return iou_score
    
    def train_knowledge_distillation(self, train_loader, test_loader, epochs, learning_rate, T, soft_target_loss_weight, hard_target_loss_weight):
        self.dice_loss = smp.losses.DiceLoss(smp.losses.BINARY_MODE, from_logits=True)
        self.ce_loss = nn.CrossEntropyLoss()
        self.clf = self.model.to(self.device)
        # self.clf.train()
        optimizer = optim.SGD(self.clf.parameters(), **self.params['optimizer_args'])
        prog_bar = tqdm.tqdm(range(1, epochs + 1), ncols=100, disable=False)
        for epoch in prog_bar:
            self.clf.train() # Student to train mode
            running_loss = []
            indicies = []
            for batch_idx, (inputs, labels, logits, index) in enumerate(train_loader):
                inputs, labels, teacher_logits = inputs.to(self.device), labels.to(self.device), logits.to(self.device)
                teacher_logits.requires_grad = False

                optimizer.zero_grad()
                
                student_logits = self.clf(inputs)
                
                #Soften the logits by applying sigmoid
                soft_targets = nn.functional.sigmoid(teacher_logits)
                soft_student_logits = nn.functional.sigmoid(student_logits)
                
                soft_targets_loss = self.ce_loss(soft_student_logits, soft_targets)
    
                # Calculate the true label loss
                label_loss = self.dice_loss(student_logits, labels)
                
                # Weighted sum of the two losses
                loss = soft_target_loss_weight * soft_targets_loss + hard_target_loss_weight * label_loss
    
                loss.backward()
                optimizer.step()
                running_loss.append(loss.item())
                prog_bar.set_postfix(loss =  loss.item())
            train_loss = sum(running_loss) / len(running_loss)
            iou = self.test(test_loader)
            # wandb.log({"train_kd_loss" : train_loss})
            wandb.log({"train_kd_loss" : train_loss, "val_iou":iou})
            # print(f"Epoch {epoch+1}/{epochs}, Loss: {train_loss}")
                    


    def train(self, train_loader, test_loader, epochs, learning_rate):
        self.dice_loss = smp.losses.DiceLoss(smp.losses.BINARY_MODE, from_logits=True)
        self.clf = self.model.to(self.device)
        # self.clf.train()
        optimizer = optim.SGD(self.clf.parameters(), **self.params['optimizer_args'])    
        
        prog_bar = tqdm.tqdm(range(1, epochs + 1), ncols=100, disable=False)
        
        for epoch in prog_bar:
            self.clf.train()
            running_loss = []
            for batch_idx, (inputs, labels, _, _) in enumerate(train_loader):
                inputs, labels = inputs.to(self.device), labels.to(self.device)    
                optimizer.zero_grad()
                outputs = self.clf(inputs)                
                loss = self.dice_loss(outputs, labels)
                running_loss.append(loss.item())
                loss.backward()
                optimizer.step()
                prog_bar.set_postfix(loss =  loss.item())
            train_loss = sum(running_loss) / len(running_loss)
            iou = self.test(test_loader)
            # wandb.log({"train_kd_loss" : train_loss})
            wandb.log({"train_kd_loss" : train_loss, "val_iou":iou})
            # print(f"Epoch {epoch+1}/{epochs}, Loss: {train_loss}")
                
    

    def predict(self, data):
        """
        Generates predictions using the trained model.

        Args:
            data: The input dataset for prediction.

        Returns:
            preds: Predicted masks.
            masks: Ground truth masks.
        """
        self.clf.eval()
        preds = torch.zeros(len(data), self.image_dim, self.image_dim)    
        masks = torch.zeros(len(data), self.image_dim, self.image_dim)
        loader = DataLoader(data, shuffle=False, **self.params['test_args'])
        with torch.no_grad():
            for x, y, _, idxs in loader:
                x, y = x.to(self.device), y.to(self.device)
                out = self.clf(x)                    
                preds[idxs] = out.squeeze().cpu()
                masks[idxs] = y.squeeze().cpu()

        return preds, masks

class RandomSampling():
    def __init__(self, dataset):
        """
        Initializes the RandomSampling strategy.

        Args:
            dataset: The dataset object.
        """
        self.dataset = dataset

    def update(self, pos_idxs):
        """
        Updates the seen indices in the dataset.

        Args:
            pos_idxs (ndarray): The indices of positively seen samples.            
        """
        self.dataset.seen_idxs[pos_idxs] = True

    def query(self, n):
        """
        Selects a subset of unseen samples randomly for labeling.

        Args:
            n (int): The number of samples to query.

        Returns:
            ndarray: The indices of the selected samples.
        """
        return np.random.choice(np.where(self.dataset.seen_idxs == 0)[0], n, replace=False)