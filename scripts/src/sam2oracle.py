import torch
import os
import pickle
import numpy as np
import matplotlib.pyplot as plt
import sys
import pandas as pd
import cv2
import numpy as np
import supervision as sv

from os.path import expanduser
home = expanduser("~")

sam_2_path = home+'/segment-anything-2'
sys.path.append(sam_2_path)
from PIL import Image
from sam2.build_sam import build_sam2
from sam2.sam2_image_predictor import SAM2ImagePredictor
from sam2.automatic_mask_generator import SAM2AutomaticMaskGenerator


class SAM2Oracle():
    
    def __init__(self,
                 device = torch.device("cuda" if torch.cuda.is_available() else "cpu"),
                 model_cfg = "sam2_hiera_l.yaml",
                 checkpoint_path = sam_2_path + "/checkpoints/sam2_hiera_large.pt",
                 model = None,
                 img_size=(256, 256)
                ):
        self.img_size = img_size
        self.device = device
        self.model_cfg = model_cfg
        self.checkpoint_path = checkpoint_path
        self.model = build_sam2(self.model_cfg, self.checkpoint_path, device=self.device)
        self.mask_predictor = SAM2ImagePredictor(self.model)
        self.mask_generator = SAM2AutomaticMaskGenerator(self.model,pred_iou_thresh=0.3)                                            
        
        
    def get_boxes(self, mask):
        if torch.is_tensor(mask):
            mask = mask.numpy()
            mask = np.array(mask, np.uint8)
        # _, thresh = cv2.threshold(mask, 0.5, 1, 0)
        contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        cnts = []
        for cnt in contours:
            x,y,w,h = cv2.boundingRect(cnt)
            box = np.array([x, y, x+w, y+h])
            cnts.append(box)
        return np.array(cnts)   

    def get_mask(self, img_path = None, img_rgb=None, mask=None,boxes=[], multimask_output=True):
        if len(boxes) == 0:
            return np.zeros((self.img_size[0], self.img_size[1]), dtype=np.uint8)
        else:
            if img_rgb is None:
                try:
                    if img_path.endswith("npy"):
                        img_rgb = np.load(img_path, allow_pickle=True)
                        if img_rgb.shape[0]!=self.img_size[0]:
                            img_rgb=cv2.resize(img_rgb, self.img_size, interpolation=cv2.INTER_CUBIC)
                    else:
                        image_bgr = cv2.imread(img_path)
                        resized = cv2.resize(image_bgr, self.img_size, interpolation=cv2.INTER_CUBIC)
                        img_rgb = cv2.cvtColor(resized, cv2.COLOR_BGR2RGB)
                except:
                    print(img_path)
                    image_bgr = cv2.imread(img_path)
                    img_rgb = cv2.cvtColor(image_bgr, cv2.COLOR_BGR2RGB)
                
            self.mask_predictor.set_image(img_rgb)
            
            masks, scores, _ = self.mask_predictor.predict(
                point_coords=None,
                point_labels=None,
                box=boxes,
                multimask_output=multimask_output,
                )
            best_matches = []
            if scores.ndim==1:
                scores = scores.reshape(1, -1)
                masks = masks.reshape((1, 3, self.img_size[0], self.img_size[1]))
            for i in range(len(scores)):
                best_matches .append(masks[i][np.argmax(scores[i])])
            
            mask = sum(best_matches)
            mask = np.array(mask>0, dtype=np.uint8)
            return mask
            
    def generateMasks(self, img_path):
        img_rgb = np.load(img_path, allow_pickle=True)
        if img_rgb.shape[0]!=self.img_size[0]:
            img_rgb=cv2.resize(img_rgb, self.img_size, interpolation=cv2.INTER_CUBIC)
        generated_masks = self.mask_generator.generate(img_rgb)
        list_generated_masks=[]
        for mask in generated_masks:
            list_generated_masks.append(mask["segmentation"])
        return list_generated_masks
 
    
    def drawBox(self, box, ax):
        x0, y0 = box[0], box[1]
        w, h = box[2] - box[0], box[3] - box[1]
        ax.add_patch(plt.Rectangle((x0, y0), w, h, edgecolor='white', facecolor=(0,0,0,0), lw=2))    
    
    def showBox(self, img_path:str, boxes=[]):
        if len(boxes) == 0:
            print("Boxes are empty!")
            return np.zeros((1, self.img_size[0], self.img_size[1]), dtype=np.uint8)

        image_bgr = cv2.imread(img_path)
        img_rgb = cv2.cvtColor(image_bgr, cv2.COLOR_BGR2RGB)
        mask = self.get_mask(img_rgb=img_rgb, boxes=boxes)
        plt.figure(figsize=(5, 5))
        plt.imshow(gt_mask)
        self.show_box(input_box[0], plt.gca())
        plt.axis('off')
        plt.show()