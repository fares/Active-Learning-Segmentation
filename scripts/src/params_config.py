def configParams(params, train_df, test_df, notebooks_path):
    if params["init_set_size"] == len(train_df):
        params["training_type"] = "no_active"
    elif not params["use_sam"]:
        params["training_type"] = "no_sam"
        name = "NoSAM"
    elif params["voting"]:
        params["training_type"] = "voters"
        name = "Voters"
    else:
        params["training_type"]="withSAM_NoVoting"
        name = "NoVoting"
    
    if params["training_type"] == "no_active":
        if params["pre_trained"]:
            params["model_path"] = f'{notebooks_path}trained_models/{params["dataset"]}/no_active/pre_trained{params["img_size"][0]}.pt'
        else:
            params["model_path"] = f'{notebooks_path}trained_models/{params["dataset"]}/no_active/not_pre_trained{params["img_size"][0]}.pt'
    else:
        if params["pre_trained"]:
            params["model_path"] = f'{notebooks_path}trained_models/{params["dataset"]}/{params["training_type"]}/pre_trained/{params["init_set_size"]}'
        else:
            params["model_path"] = f'{notebooks_path}trained_models/{params["dataset"]}/{params["training_type"]}/not_pre_trained/{params["init_set_size"]}'
    
    notes = f'{params["training_type"]}_{params["init_set_size"]}'
    if params["dropout"] and params["training_type"] != "voters":
        params["model_path"] = f'{params["model_path"]}_dropout'
        notes = f"{notes}_dropout"
    
    if params["use_generator"] and params["training_type"] != "voters":
        params["model_path"] = f'{params["model_path"]}_generator'
        notes = f"{notes}_generator"
        name = "SAM_Generator"
        if params["sample_rejection"]:
            params["model_path"] = f'{params["model_path"]}_rejection'
            notes = f"{notes}_rejection"
            name = f"{name}_Rejection"
        else:
            params["model_path"] = f'{params["model_path"]}_no_rejection'
            notes = f"{notes}_no_rejection"
            name = f"{name}_Predictor"
    
    params['test_set_size'] = len(test_df)
    if params['query_num'] == 0:
        params['query_num'] = 1
    params["strategy"] = "MarginSampling"
    
    if params["training_type"] == "voters":
        if params["similarity_check"]:
            params["model_path"] = f'{params["model_path"]}_dbscan'
            notes = f"{notes}_dbscan"
            name = f"{name}_DBScan"
        elif params["similarity_learning"]:
            params["model_path"] = f'{params["model_path"]}_similarity'
            notes = f"{notes}_similarity"
            name = f"{name}_SimNet"
            
        if params["dropout"]:
            params["model_path"] = f'{params["model_path"]}_dropout'
        if params["use_generator"]:
            params["model_path"] = f'{params["model_path"]}_generator'
            notes = f"{notes}_generator"
            name = "SAM_Generator"
            if params["sample_rejection"]:
                params["model_path"] = f'{params["model_path"]}_rejection'
                notes = f"{notes}_rejection"
                name = f"{name}_Rejection"
            else:
                params["model_path"] = f'{params["model_path"]}_no_rejection'
                notes = f"{notes}_no_rejection"
                if params["similarity_check"]:
                    name = f"{name}_DBScan"
                
        
        params["model_path"] = f'{params["model_path"]}/voters_{params["img_size"][0]}'

    if params["pre_trained"]:
        params["init_state_Unet_path"] = notebooks_path+"trained_models/shared_init_state_pre_trained"
        params["first_rd_Unet_path"] = notebooks_path+f"trained_models/shared_1st_state_pre_trained_{params['init_set_size']}"
    else:
        params["init_state_Unet_path"] = notebooks_path+"trained_models/shared_init_state_not_trained"
        tmp = "trained_models/shared_1st_state_not_trained"
        if params["dropout"]:
            tmp= f"{tmp}_dropout"
        params["first_rd_Unet_path"] = notebooks_path+f"{tmp}_{params['init_set_size']}"
    
    params["init_state_Unet_path"] = params["init_state_Unet_path"] + f"_{params['seed']}_{params['n_classes']}.pt"
    params["first_rd_Unet_path"] = params["first_rd_Unet_path"] + f"_{params['seed']}_{params['n_classes']}.pt"
    name = f"{name}_{params['init_set_size']}"

    if params["pre_trained"]:
        notes = f"{notes}_pre_trained"
    else:
        notes = f"{notes}_not_pre_trained"
        
    return params, notes, name